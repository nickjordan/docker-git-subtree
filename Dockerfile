FROM docker:git
MAINTAINER Nick Jordan <nick@nljordan.com>
RUN apk add --no-cache git-subtree
